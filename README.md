Written by Erica Keeble, Riley Campbell, and Matthew White

Prog4.cpp contains main() and the doxygen documentation. 

Belt.h contains "conveyorbelt" class declaration and the "box" struct

utilities.h contains function declarations for various functions used throughout the program

unsortedDouble.h contains "unsortedDouble" class declaration and implementation

erica.cpp contains my implementation of the conveyorbelt class and functions for utilities.h

testCases.cpp contains my test cases using catch.cpp

matt_Belt.cpp contains Matthew White's implementation of conveyorBelt class

Riley_utilities.cpp contains Riley Campbell's implementation of utilities functions 

Matt.cpp contains implementation of 1 utilities function